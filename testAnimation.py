import sys
sys.path.append('/home/jesus/Documents/tum/SmallCodes')

import refIndexWithDoping2 as RI
import MaxwellBloch.MaxwellClass as MClass
import MaxwellBloch.Maxwell as MAX
import MaxwellBloch.testMaxwell as test
import MaxwellBloch.animationClass as animationClass

import numpy as np                                   
import matplotlib.pyplot as plt                      
from matplotlib.animation import FuncAnimation

def test1(numPlots=2):
    """
    Running animation from outside the class
    """
    global anim
    def fun1(i):
        x = np.linspace(0, 2*np.pi, 100)
        y = np.sin(x - 0.01*i)
        return [(x, y)]

    def fun2(i):
        x = np.linspace(0, 2*np.pi, 100)
        y1 = np.sin(x - 0.01*i)
        y2 = np.cos(x - 0.01*i)
        return [(x, y1), (x, y2)]

    fig, ax = plt.subplots()

    if(numPlots==1): ax.plot([], []); func = fun1
    elif(numPlots==2): ax.plot([], [], [], []); func = fun2
    else: return None 

    ax.axis([0, 2*np.pi, -1, 1])
    myAnim = animationClass.Animation(ax, func)

    anim = FuncAnimation(myAnim.fig, myAnim, frames=np.arange(100), init_func=myAnim.init,  
                         interval=50, blit=True)

    plt.show()


def test2(numPlots=2):
    """
    Running animation using the corresponding animationClass method
    """
    def fun1(i):
        x = np.linspace(0, 2*np.pi, 100)
        y = np.sin(x - 0.01*i)
        return [(x, y)]

    def fun2(i):
        x = np.linspace(0, 2*np.pi, 100)
        y1 = np.sin(x - 0.01*i)
        y2 = np.cos(x - 0.02*i)
        return [(x, y1), (x, y2)]

    fig, ax = plt.subplots()

    if(numPlots==1): ax.plot([], [], lw=2); func = fun1
    elif(numPlots==2): ax.plot([], [], [], [], lw=2); func = fun2
    else: return None 

    ax.axis([0, 2*np.pi, -1, 1])
    myAnim = animationClass.Animation(ax, func)

    myAnim.runAnimation(np.arange(100))

    plt.show()

def test3():
    """
    Running animation that dynamically updates Y limits
    """
    def func(i):
        x = np.linspace(0, 2*np.pi, 100)
        y1 = np.sin(x - 0.01*i)
        y2 = (1+0.01*i)*np.cos(x - 0.02*i)
        return [(x, y1), (x, y2)]

    fig, ax = plt.subplots()

    ax.plot([], [], [], [], lw=2)

    ax.axis([0, 2*np.pi, -1.5, 1.5])
    myAnim = animationClass.Animation(ax, func, False)
    myAnim.runAnimation(np.arange(100))

    plt.show()

def test4(timeStep=750, r1=0.8, r2=0.8, k=0, method='Lax-Wendroff', 
          animate='fieldAmps', interval=50, blit=True):
    """
    Runs animation for test case

    Input:
        animate (string) -> Animate field amplitudes ('fieldAmps'), total
                            electric field ('E'), or just perform plot (False) 
    """
    def plotAmplitudes(Maxwell, ax):
        title='Field amplitudes using method: ' + Maxwell.get_method()
        xlabel='$x$'
        M = Maxwell.get_M()
        text = 'Intervals: %d'%M

        timeStep = Maxwell.get_timeStep()
        label1 = r'$\mathrm{Re} \left( E_{-} \right)$ at j = ' + str(timeStep)
        label2 = r'$\mathrm{Re} \left( E_{+} \right)$ at j = ' + str(timeStep)

        labels1 = {'title':title,'xlabel':xlabel, 'label':label1, 'text':text}
        labels2 = {'label':label2}

        x = Maxwell.get_x()
        E_left = Maxwell.event.get_E_left()
        E_right = Maxwell.event.get_E_right()

        MAX.plot(x, np.real(E_left[1:-1]), **labels1, ax=ax )
        MAX.plot(x, np.real(E_right[1:-1]), **labels2, loc='lower left', ax=ax )

    def initializeMaxwell(Maxwell=None):
        #Simulation Parameters
        M, N, length, simTime, deltaX, deltaT, w, timeRoundtrip, roundtrips = test.testParams()

        #Initialize Event
        F = np.zeros(M+3, dtype=complex)
        F[1:-1] = MAX.initial_f_test(M, length, w)
        DtF = np.zeros(M+3, dtype=complex)
        DtF[1:-1] = MAX.Dt_f(0, case='test')
        event = MClass.Event(M)
        event.set_timeStep(0)
        event.set_F(F)
        event.set_DtF(DtF)

        if(Maxwell==None):
            #Create Scenario
            material = RI.Vacuum()
            laser = MClass.Laser( length, material )
            scenario = MClass.Scenario(laser, k, r1, r2, freq=w/(2*np.pi))

            #Initialize Maxwell Simulation
            MaxwellSim = MClass.Maxwell(N, simTime, scenario, event, method=method)
            MaxwellSim.set_initial_E(dist='zero')
            MaxwellSim.event.updateEventStatus()
            return MaxwellSim
        else:
            Maxwell.set_event(event)
            Maxwell.set_initial_E(dist='zero')
            Maxwell.event.updateEventStatus()


    def updateMaxwell(timeStep, Maxwell):
        """
        Updates Maxwell instance to a given time step
        """
        #Perform as many updates as required
        updatesRequired = timeStep - Maxwell.get_timeStep()
        if(updatesRequired==0): return
        elif(updatesRequired>0):
            x = Maxwell.get_x()
            w = Maxwell.scenario.get_w()
            M = Maxwell.get_M()
            F = np.zeros(M+3, dtype=complex)
            DtF = np.zeros(M+3, dtype=complex)
            for j in range( updatesRequired ):
#                print('inside j')
                Maxwell.fieldAmplitudesUpdate()
                t = Maxwell.get_t()
                F[1:-1] = test.fun_f(x, t, w)
                DtF[1:-1] = MAX.Dt_f(Maxwell.get_timeStep(), case='test')
                Maxwell.event.set_F( F )
                Maxwell.event.set_DtF( DtF )
                Maxwell.event.updateEventStatus()
        #If required updates are negative, start over
        else:
#            print('inside else')
            initializeMaxwell(Maxwell)
            updateMaxwell(timeStep, Maxwell)
    
    def fieldAmplitudes(timeStep):
        updateMaxwell(timeStep, Maxwell)
        x = Maxwell.get_x()
        E_left = Maxwell.event.get_E_left()
        E_right = Maxwell.event.get_E_right()
        return [ (x, np.real(E_left[1:-1])),  (x, np.real(E_right[1:-1])) ]


    def Efield(timeStep):
        updateMaxwell(timeStep, Maxwell)
        x = Maxwell.get_x()
        E = Maxwell.get_E() 
        return [ (x, E) ]


    def plotFieldAmplitudes(timeStep):
        updateMaxwell(timeStep, Maxwell)
        ax = MAX.plot([], [])
        plotAmplitudes(Maxwell, ax)


    def plotEfield(timeStep):
        updateMaxwell(timeStep, Maxwell)
        x = Maxwell.get_x()
        E = Maxwell.get_E() 
        j = Maxwell.get_timeStep()
        M = Maxwell.get_M()
        MAX.plot(x, E,"$x$","$E$", "Total electric field at time step %d"%j, text='Intervals: %d'%M)


    Maxwell = initializeMaxwell()

    if(animate=='fieldAmps'):
        title='Field amplitudes using method: ' + Maxwell.get_method()
        xlabel='$x$'
        M = Maxwell.get_M()
        text = 'Intervals: %d'%M
        loc = 'lower left'

        label1 = r'$\mathrm{Re} \left( E_{-} \right)$' 
        label2 = r'$\mathrm{Re} \left( E_{+} \right)$' 

        fig, ax = plt.subplots()
        ax.plot([], [], lw=2, label=label1)
        ax.plot([], [], lw=2, label=label2)
        ax.set_title(title)
        ax.set_xlabel(xlabel)

        withTextBox = False

        if(withTextBox):
            style=dict(fontsize=15)
            bbox=dict(facecolor='orange', edgecolor='none', alpha=0.5, boxstyle="round")
            ax.text(0.02,0.92, text, **style, bbox=bbox, transform=ax.transAxes)

        ax.axis([0, Maxwell.scenario.laser.get_length(), -2e-18, 2e-18])
        ax.legend(frameon=False, loc=loc)

        myAnim = animationClass.Animation(ax, fieldAmplitudes, YaxDimFixed=False)
        myAnim.runAnimation(frames=np.arange(timeStep), interval=interval, blit=blit)

        plt.show()

#        fig, ax = plt.subplots()
#        ax.plot([], [], [], [], lw=2)
#
#        ax.axis([0, Maxwell.scenario.laser.get_length(), -2e-18, 2e-18])
#        myAnim = animationClass.Animation(ax, fieldAmplitudes, False)
#        myAnim.runAnimation(frames=np.arange(timeStep), interval=interval, blit=blit)
#
#        plt.show()

    elif(animate=='E'):
        title='Total electric field'

        fig, ax = plt.subplots()
        ax.plot([], [], lw=2)
       
        ax.set_title(title)
        ax.set_xlabel('$x$')
        ax.set_ylabel('$E$')

        ax.axis([0, Maxwell.scenario.laser.get_length(), -1e-16, 1e-16])
        myAnim = animationClass.Animation(ax, Efield, YaxDimFixed=False)
        myAnim.runAnimation(frames=np.arange(timeStep), interval=interval, blit=blit)

        plt.show()

    elif(not animate):
        plotFieldAmplitudes(timeStep)
        plotEfield(timeStep)

if(__name__=='__main__'):
    test4()
