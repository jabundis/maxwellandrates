import sys
sys.path.append('/home/jesus/Documents/tum/SmallCodes')

import numpy as np
import matplotlib.pyplot as plt
import fundamentalConstants as FC
#from testMaxwell import testParams
import testMaxwell as test


#def testParams():
#    length = 2.0e-4 # m
#    roundtrips = 10
#    M = 500
#    f = 25e12  # s^(-1) 
#
#    w =  2*np.pi*f
#    N, deltaX, deltaT, simTime, timeRoundtrip = times(length, roundtrips, M)
#    return (M, N, length, simTime, deltaX, deltaT, w, timeRoundtrip, roundtrips)
#
#
#def times(length, roundtrips, M, n=1, c=FC.c):
#    """
#    M (int) -> Number of intervals in half a roundtrip
#    """
#    v = c/n
#    timeRoundtrip = (2*length) / v
#    simTime = roundtrips * timeRoundtrip
#    N = (2*M) * roundtrips
#    deltaX = length / M
#    deltaT = simTime / N
#    return (N, deltaX, deltaT, simTime, timeRoundtrip)


def initial_E(M,dist=None, loc=None, scale=None):
    """
    Returns initial Electric field along all "M+1" gridpoints over the length
    (E(i,0) for all i=0...M) as a numpy 1D array.

    If "dist" is provided, the initial field is determined
    from requested sthocastic distribution, otherwise it provides
    a Gaussian pulse with localization at the gridpoints center and scale
    of 5% the interval.

    Input:
        dist (string, optional) -> distribution requested.
                                   possibilities: 'zero','gaussian_noise'.
                                   If not provided, Gaussian pulse provided (see above)

    Output:
        Electric field as a 1D np.array with M+1 elements
    """
    if(dist=='zero'): return np.zeros(M+1, dtype=complex)
    elif(dist=='gaussian_noise'):
        if(loc==None): loc = 0.1
        if(scale==None): scale = 0.01
        return np.random.normal(loc, scale, size=M+1)
    elif(dist==None):
        if(loc==None): loc = 0.5
        if(scale==None): scale = 0.05
        x = np.linspace(0,1,M+1)
        E = 1.0/(np.sqrt(2 * np.pi) * scale) * np.exp(-(x-loc)**2 / (2.0*scale**2) )
        return E

def initial_f_test(M, length, w, c=FC.c):
    """
    Returns initial function f along all "M+1" gridpoints over the length
    (f(i,0) for all i=0...M) as a numpy 1D array.
    """
    x = np.linspace(0,1,M+1) * length
    k = w / c
    f = x * np.exp(1j * k * x)
    return f

def timeDerivative_f_test(j, M, N, simTime, length, w, c=FC.c):
    """
    Returns time derivative of test function "f" along all "M+1" gridpoints 
    over the length at time step j (j = 0...N) as a 1D numpy array.
    """
    x = np.linspace(0,1,M+1) * length
    t = (j/float(N)) * simTime 
    k = w / c
    Dt_f = np.exp(1j * (k*x - w*t)) * (c - 1j*w*(x + c*t) )
    return Dt_f

def Dt_f(j, case='test'):
    """
    Returns time derivative of "f" at time step "j" (j = 0,1,...,N) along all
    "M+1" gridpoints over the length as a 1D ndarray. If case="test", parameters
    from function "testParams" are used.
    """
    if(case=='test'):
        M, N, length, simTime, deltaX, deltaT, w, timeRoundtrip, roundtrips = test.testParams()
        Dtf = timeDerivative_f_test(j, M, N, simTime, length, w)
        return Dtf
        

def Dx_A(A, deltaX, method='centralDif', boundaries=True, points='four', beyondLim=None):
    """
    Returns numerical derivative of 1D ndarray "A" usign a specified "method" and
    assuming a constant coordinate separation "deltaX". 
    
    If "boundaries" is set to True, derivative at the boundaries is calculated 
    using "two" or "four" points as specified by "points". If set to False, the 
    boundaries of the returned derivative array are set to NaN.

    If method is centralDif and boundaries is set to True, derivative at the
    boundaries can be calculated from data just outside the simulation window
    given in the tuple beyondLim. If "beyondLim" is given, it takes 
    preference over "points".

    Input:
        A (1D ndarray) -> Field of interest along the coordinate for derivative
        deltaX (float) -> Separation between neighbor coordinates (e.g., x(1) - x(0))
        method (string) -> 'centralDif'
        boundaries (bool) -> Defines whether to calculate derivative at boundaries
        points (string) -> Points used for derivative at boundaries 
                           Can be set to either "two" or "four"
        beyondLim (left, right) -> tuple with data outside simulation window

    Returns:
        Dx (1d ndarray) -> first order derivative of "A" of same length
    """
#    def left(B):
#        return (B[1] - B[0]) / deltaX
#    def right(B):
#        return (B[-1] - B[-2]) / deltaX
    def boundariesDer(B, points):
        """
        First derivative at the boundaries of matrix B
        """
#        return (0.5 * B[1], 0.5 * B[-2]) 
        if(points=='two'): 
            left = (B[1] - B[0]) / deltaX
            right = (B[-1] - B[-2]) / deltaX
            return (left, right)
        elif(points=='four'): 
            left = (1.0/deltaX) * ( B[2] - B[0] - 0.5*(B[3]-B[1]) )
            right = (1.0/deltaX) * ( B[-1] - B[-3] - 0.5*(B[-2]-B[-4]) )
            return (left, right)

    method = method.lower()
    points = points.lower()
    if(method == 'centraldif'):
        Dx = np.zeros(len(A), dtype=complex)
        #Derivative at inner points
        Dx[1:-1] = ( A[2:] - A[:-2] ) / (2.0 * deltaX)
        #Deal with boundaries
        if(boundaries): 
            if(beyondLim): 
                left = beyondLim[0]; right = beyondLim[1]
                Dx[0] = ( A[1] - left ) / (2.0 * deltaX)
                Dx[-1] = ( right - A[-2] ) / (2.0 * deltaX)
                return Dx
            Dx[0], Dx[-1] = boundariesDer(A, points)
        else: 
            Dx[0], Dx[-1] = (np.NaN, np.NaN)
    return Dx


def Dxx_A(A, deltaX, method='centralDif', boundaries=True, beyondLim=None):
    """
    Returns second order numerical derivative of 1D ndarray "A" usign a specified 
    "method" and assuming a constant coordinate separation "deltaX".

    If "boundaries" is set to True, second derivative at the boundaries is 
    calculated. If set to False, the boundaries of the returned derivative array 
    are set to NaN.

    If method is centralDif and boundaries is set to True, the second derivative 
    at the boundaries can be calculated from data just outside the simulation 
    window given in the tuple beyondLim or from neighbor points in case beyondLim
    is set no None. 

    Input:
        A (1D ndarray) -> Field of interest along the coordinate for derivative
        deltaX (float) -> Separation between neighbor coordinates (e.g., x(1) - x(0))
        method (string) -> 'centralDif'
        boundaries (bool) -> Defines whether to calculate derivative at boundaries
        beyondLim (left, right) -> tuple with data outside simulation window

    Returns:
        Dxx (1d ndarray) -> second order derivative of "A"
    """
    def boundaries2Der(B):
        return (0.5 * B[1], 0.5 * B[-2]) 

    method = method.lower()
    if(method == 'centraldif'):
        Dxx = np.zeros(len(A), dtype=complex)
        Dxx[1:-1] = ( A[2:] - 2*A[1:-1]  + A[:-2] ) / (deltaX**2)
        if(boundaries): 
            if(beyondLim): 
                left = beyondLim[0]; right = beyondLim[1]
                Dxx[0] = ( A[1] - 2*A[0] + left ) / (deltaX**2)
                Dxx[-1] = ( right - 2*A[-1] + A[-2] ) / (deltaX**2)
                return Dxx
            Dxx[0], Dxx[-1] = boundaries2Der(Dxx)
        else: Dxx[0], Dxx[-1] = (np.NaN, np.NaN)
    return Dxx


def update_LaxWen(E, f, Dt_f, DxE, DxxE, Dxf, deltaT, k, c=FC.c, direction='forward'):
    """
    Returns 1D numpy array with electric field amplitude in given "direction" along 
    space gridpoints at time  step "n+1" given system state at time step "n", using 
    the "Lax-Wendroff" method.
    
    If "boundaries" set to True the derivatives at the edges are calculated using four points.
    Otherwise, electric field at the edges is set to NaN.

    Input:
        E (1D ndarray) -> Electric field at time step "n"
        f (1D ndarray) -> "f" function at time step "n"
        Dt_f (1D ndarray) -> Time derivative of "f" at time step "n"
        DxE (1D ndarray) -> Space derivative of "E" at time step "n"
        DxxE (1D ndarray) -> Second space derivative of "E" at time step "n"
        Dxf (1D ndarray) -> Space derivative of "f" at time step "n"
        deltaT (float) -> Time interval [s]
        k (float)   -> Factor in the wave equation term: k*E (not the wavenumber)
        c (float) -> Vacuum light speed [m/s] 
        direction (string) -> "forward"  : E field propagating in positive space direction
                              "backwards": "   "        "      "  negative   "       "
    """
    direction = direction.lower()
    if(direction=='forward'):     c = -c 
    elif(direction=='backwards'): c = c
    else: raise Exception('choose right "direction"')

    newE = E  +  deltaT * (1 + 0.5*k*deltaT) * (c*DxE + f + k*E)  +  \
           0.5*deltaT**2 * (c**2 * DxxE + c*k*DxE + c*Dxf + Dt_f)

    return newE


def oneWaveUpdate_LaxWen(E, f, Dt_f, deltaX, deltaT, k, c=FC.c, direction='forward', boundaries=True):
    """
    Returns 1D numpy array with electric field amplitude in given "direction" along 
    space gridpoints at time  step "n+1" given system state at time step "n", using 
    the "Lax-Wendroff" method.
    
    If "boundaries" set to True the derivatives at the edges are calculated using four points.
    Otherwise, electric field at the edges is set to NaN.

    Input:
        E (1D ndarray) -> Electric field at time step "n"
        f (1D ndarray) -> "f" function at time step "n"
        Dt_f (1D ndarray) -> Time derivative of "f" at time step "n"
        deltaX (float) -> Space interval [m]
        deltaT (float) -> Time interval [s]
        k (float)   -> Factor in the wave equation term: k*E (not the wavenumber)
        c (float) -> Vacuum light speed [m/s] 
        direction (string) -> "forward"  : E field propagating in positive space direction
                              "backwards": "   "        "      "  negative   "       "
        boundaries (bool) -> Defines whether to calculate electric field at boundaries 
    """
    direction = direction.lower()
    if(direction=='forward'):     c = -c 
    elif(direction=='backwards'): c = c
    else: raise Exception('choose right "direction"')

    DxE = Dx_A(E, deltaX, 'centralDif', boundaries)
    DxxE = Dxx_A(E, deltaX, 'centralDif', boundaries) 
    Dxf = Dx_A(f, deltaX, 'centralDif', boundaries)

    newE = E  +  deltaT * (1 + 0.5*k*deltaT) * (c*DxE + f + k*E)  +  \
           0.5*deltaT**2 * (c**2 * DxxE + c*k*DxE + c*Dxf + Dt_f)

    return newE


def electricUpdate_RNFD(E, f, Dt_f, deltaX, deltaT, k, c=FC.c, direction='forward', boundaries=True):
    """
    Returns 1D numpy array with electric field amplitude in given "direction" along 
    space gridpoints at time  step "n+1" given system state at time step "n", using 
    the "RNFD" method.
    
    If "boundaries" set to True the derivatives at the edges are calculated using four points.
    Otherwise, electric field at the edges is set to NaN.

    Input:
        E (1D ndarray) -> Electric field at time step "n"
        f (1D ndarray) -> "f" function at time step "n"
        Dt_f (1D ndarray) -> Time derivative of "f" at time step "n"
        deltaX (float) -> Space interval [m]
        deltaT (float) -> Time interval [s]
        k (float)   -> Factor in the wave equation term: k*E (not the wavenumber)
        c (float) -> Vacuum light speed [m/s] 
        direction (string) -> "forward"  : E field propagating in positive space direction
                              "backwards": "   "        "      "  negative   "       "
        boundaries (bool) -> Defines whether to calculate electric field at boundaries 
    """
    pass


def fieldAmplitudesUpdate_v1(E_left, E_right, f, Dt_f, r1, r2, deltaX, deltaT, k, 
                            c=FC.c, method='Lax-Wendroff'):
    """
    Updates electric field amplitudes for right and left propagating waves along 
    space grid points for time step "n+1" given system state at time step "n", 
    using the given method. 

    It assumes E_right=r1*E_left at x=0 and E_left=r2*E_right at x=M. 

    ______________________________________________________________________________
    Case: method=Lax-Wendroff
    For speed purposes it assumes we provide an extra point beyond each simulation 
    window boundary for E_left, E_right, f, and Dtf, whose actual values are ignored
    (internally reset according to the boundary conditions: (E+)=r1*(E-) at x0 and 
    (E-)=r2*(E+) at xM ). 
    An example of valid f: [np.NaN, f(0),..., f(M), np.NaN].
    ______________________________________________________________________________
    
    Input:
        E_left (1D ndarray) -> Left electric field amplitude at time step "n" along 
                               space grid points 
        E_right (1D ndarray) -> Right electric field amplitude at time step "n" along
                                space grid points 
        f (1D ndarray) -> "f" function at time step "n"
        Dt_f (1D ndarray) -> Time derivative of "f" at time step "n"
        r1 (float) -> Reflectance at left interface (from 0 to 1)
        r2 (float) -> Reflectance at right interface (from 0 to 1)
        deltaX (float) -> Space interval [m]
        deltaT (float) -> Time interval [s]
        k (float)   -> Factor in the wave equation term: k*E (not the wavenumber)
        c (float) -> Vacuum light speed [m/s] 
        direction (string) -> "forward"  : E field propagating in positive space direction
                              "backwards": "   "        "      "  negative   "       "
        boundaries (bool) -> Defines whether to calculate electric field at boundaries 

    Returns:
        (Enew_left, Enew_right) -> Updated left and right electric field amplitudes. The points
                    beyond the boundaries are set to NaN, i.e. np.array([NaN, Enew(0),..., Enew(M), NaN])

                    Enew_left and Enew_right are 1d ndarrays.
    """
    def Eboundary(E_left, E_right):
        def derX_E():
            factor1 = 1.0/(2*c) * f[1] * (1-r1)
            factor2 = 1.0/(2*c) * f[-2] * (r2-1)
            derEleft_x0 = factor1/r1
            derEright_x0 = factor1
            derEleft_xM = factor2
            derEright_xM = factor2/r2
            return ( derEleft_x0, derEright_x0, derEleft_xM, derEright_xM )
#        if( (E_left[0] is np.NaN) or (E_left[-1] is np.NaN) ):
        derEleft_x0, derEright_x0, derEleft_xM, derEright_xM = derX_E()
        E_left[0] =  E_left[2] - 2*deltaX*derEleft_x0
        E_right[0] =  E_right[2] - 2*deltaX*derEright_x0
        E_left[-1] =  E_left[-3] + 2*deltaX*derEleft_xM
        E_right[-1] =  E_right[-3] + 2*deltaX*derEright_xM

    def fBoundary(f, points):
        if(points=='two'):
            f[0] = 2*f[1] - f[2]
            f[-1] = 2*f[-2] - f[-3]
        if(points=='four'):
            f[0] = f[4] + 2*( f[1] - f[3] )
            f[-1] = f[-5] + 2*( f[-2] - f[-4] )

    #Update the fields now
    method = method.lower()
    if(method=='lax-wendroff'):
        #Calculate and update E and f ouside simulation window
        Eboundary(E_left, E_right)
        fBoundary(f, points='two')
        #Update waves amplitudes
        Enew_left = oneWaveUpdate_LaxWen(E_left, f, Dt_f, deltaX, deltaT, k, c=FC.c, 
                                          direction='backwards', boundaries=False)

        Enew_right = oneWaveUpdate_LaxWen(E_right, f, Dt_f, deltaX, deltaT, k, c=FC.c, 
                                          direction='forward', boundaries=False)
    return (Enew_left, Enew_right)


def fieldAmplitudesUpdate_v2(E_left, E_right, f, Dt_f, r1, r2, deltaX, deltaT, k, 
                            c=FC.c, method='Lax-Wendroff'):
    """
    Version 2
    Updates electric field amplitudes for right and left propagating waves along 
    space grid points for time step "n+1" given system state at time step "n", 
    using the given method. 

    It assumes E_right=r1*E_left at x=0 and E_left=r2*E_right at x=M. 

    ______________________________________________________________________________
    Case: method=Lax-Wendroff
    For speed purposes it assumes we provide an extra point beyond each simulation 
    window boundary for E_left, E_right, f, and Dtf, whose actual values are ignored
    (internally reset according to the boundary conditions: (E+)=r1*(E-) at x0 and 
    (E-)=r2*(E+) at xM ). 
    An example of valid f: [np.NaN, f(0),..., f(M), np.NaN].
    ______________________________________________________________________________
    
    Input:
        E_left (1D ndarray) -> Left electric field amplitude at time step "n" along 
                               space grid points 
        E_right (1D ndarray) -> Right electric field amplitude at time step "n" along
                                space grid points 
        f (1D ndarray) -> "f" function at time step "n"
        Dt_f (1D ndarray) -> Time derivative of "f" at time step "n"
        r1 (float) -> Reflectance at left interface (from 0 to 1)
        r2 (float) -> Reflectance at right interface (from 0 to 1)
        deltaX (float) -> Space interval [m]
        deltaT (float) -> Time interval [s]
        k (float)   -> Factor in the wave equation term: k*E (not the wavenumber)
        c (float) -> Vacuum light speed [m/s] 
        direction (string) -> "forward"  : E field propagating in positive space direction
                              "backwards": "   "        "      "  negative   "       "
        boundaries (bool) -> Defines whether to calculate electric field at boundaries 

    Returns:
        (Enew_left, Enew_right) -> Updated left and right electric field amplitudes. The points
                    beyond the boundaries are set to NaN, i.e. np.array([NaN, Enew(0),..., Enew(M), NaN])

                    Enew_left and Enew_right are 1d ndarrays.
    """
    def Eboundary(E_left, E_right):
        def derX_E():
            factor1 = 1.0/(2*c) * f[1] * (1-r1)
            factor2 = 1.0/(2*c) * f[-2] * (r2-1)
            derEleft_x0 = factor1/r1
            derEright_xM = factor2/r2
            return ( derEleft_x0, derEright_xM )
#        if( (E_left[0] is np.NaN) or (E_left[-1] is np.NaN) ):
        derEleft_x0, derEright_xM = derX_E()
        E_left[0] =  E_left[2] - 2*deltaX*derEleft_x0
#        E_right[1] = r1 * E_left[1] 
#        E_left[-2] = r2 * E_right[-2]
        E_right[-1] =  E_right[-3] + 2*deltaX*derEright_xM

    def fBoundary(f, points):
        if(points=='two'):
            f[0] = 2*f[1] - f[2]
            f[-1] = 2*f[-2] - f[-3]
        if(points=='four'):
            f[0] = f[4] + 2*( f[1] - f[3] )
            f[-1] = f[-5] + 2*( f[-2] - f[-4] )

    #Update the fields now
    method = method.lower()
    if(method=='lax-wendroff'):
        #Calculate and update E and f ouside simulation window
        Eboundary(E_left, E_right)
        fBoundary(f, points='two')
        #Update waves amplitudes
        Enew_left = oneWaveUpdate_LaxWen(E_left, f, Dt_f, deltaX, deltaT, k, c=FC.c, 
                                          direction='backwards', boundaries=False)

        Enew_right = oneWaveUpdate_LaxWen(E_right, f, Dt_f, deltaX, deltaT, k, c=FC.c, 
                                          direction='forward', boundaries=False)

        Enew_left[-2] = r2 * Enew_right[-2]
        Enew_right[1] = r1 * Enew_left[1]
    return (Enew_left, Enew_right)


def electricField(E_left, E_right, x, t, w, n=1):
    """
    Total electric field (real numbers) by superposition of the propagating 
    pulses to the left and right.

    Input:
        E_left (1D ndarray) -> Amplitude of eletric field  propagating in 
                               left direction along position coordinates 
                                   E_left[i] -> complex number
        E_right (1D ndarray) -> Amplitude of eletric field  propagating in 
                               right direction along position coordinates
                                   E_right[i] -> complex number
        x (1D ndarray)-> positions
        t (float) -> time
        w (float) -> carrier angular frequency 
        n (float) -> refreactive index

    Returns:
        electric field (1D ndarray) -> real quantity with the 
                                  superposition of each wave times their 
                                  corresponding plane wave factor
    """
    v = FC.c/n
    beta = w/v 
    return np.real( E_right * np.exp(1j * (beta*x - w*t)) +    
                    E_left * np.exp(-1j * (beta*x + w*t)) )


#Plotting
def plot(x, y, xlabel="", ylabel="", title="", label="", text=None, ax=None, loc='best', **kwargs):
    """
    Some useful arguments:
        loc -> legend localization
    """
    if(not ax):
        fig = plt.figure(figsize=(8,6))
        ax = fig.add_subplot(111)
        if(x==[] and y==[]): return ax
#        ax = fig.add_subplot(111, facecolor='#FFFFCC')
    ax.plot(x, y, label=label, **kwargs)
    if(title): ax.set_title(title)
    if(xlabel): ax.set_xlabel(xlabel)
    if(ylabel): ax.set_ylabel(ylabel)
    if(text!=None): 
        style=dict(fontsize=15)
        bbox=dict(facecolor='orange', edgecolor='none', alpha=0.5, boxstyle="round")
        ax.text(0.02,0.92, text, **style, bbox=bbox, transform=ax.transAxes)
    plt.setp(ax.lines, linewidth=3.0)
    lg = ax.legend(frameon=False, loc=loc)
#    lg.draw_frame(False)
    return ax

def plotInitialE(M, dist=None, loc=None, scale=None):
    E = initial_E(M, dist, loc, scale)
    x = np.linspace(0,1,M+1)
    plot(x, np.real(E), '$x$', r'$\mathrm{Re} \{ E \}$')

def plot_initial_f_test(M, length, f):
#    length = 2.0e-4 # m
#    f = 25e12  # s^(-1) 

    w = 2*np.pi*f
    x = np.linspace(0,1,M+1) * length
    f = initial_f_test(M, length, w)
    plot(x, np.real(f), 'x', r'$\mathrm{Re} \{ f \}$')
