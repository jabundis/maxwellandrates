import sys
sys.path.append('/home/jesus/Documents/tum/SmallCodes')

import numpy as np
import matplotlib.pyplot as plt
import MaxwellBloch.MaxwellClass as MClass
import MaxwellBloch.testMaxwell as test
import MaxwellBloch.Maxwell as MAX
import refIndexWithDoping2 as RI


def test_electricField(j=1, r1=0.8, r2=0.8, k=0, method='Lax-Wendroff'):
    """
    Plots the final electric field and amplitudes for the test case at 
    time step "j". Required parameters are taken from "test.testParams".
    """
    def plotInitialAmplitudes(x, E_left, E_right, ax):
        title='Field amplitudes using method: '+ method
        xlabel='$x$'
        label1 = r'$\mathrm{Re} \left( E_{-} \right)$ initial'
        label2 = r'$\mathrm{Re} \left( E_{+} \right)$ initial'
        text = 'Intervals: %d'%M

        labels1 = {'title':title,'xlabel':xlabel, 'label':label1, 'text':text}
        labels2 = {'label':label2}

        MAX.plot(x, np.real(E_left[1:-1]), **labels1, ax=ax )
        MAX.plot(x, np.real(E_right[1:-1]), **labels2, ax=ax )

    def plotFinalAmplitudes(x, E_left, E_right, ax):
        label3 = r'$\mathrm{Re} \left( E_{-} \right)$ at j = ' + str(j) 
        label4 = r'$\mathrm{Re} \left( E_{+} \right)$ at j = ' + str(j)

        labels3 = {'label':label3}
        labels4 = {'label':label4}

        MAX.plot(x, np.real(E_left[1:-1]), **labels3, ax=ax )
        MAX.plot(x, np.real(E_right[1:-1]), **labels4, loc='lower left', ax=ax )


    M, N, length, simTime, deltaX, deltaT, w, timeRoundtrip, roundtrips = test.testParams()

    #Initialize Event
    F = np.zeros(M+3, dtype=complex)
    F[1:-1] = MAX.initial_f_test(M, length, w)
    DtF = np.zeros(M+3, dtype=complex)
    DtF[1:-1] = MAX.Dt_f(0, case='test')
    event = MClass.Event(M)
    event.set_timeStep(0)
    event.set_F(F)
    event.set_DtF(DtF)

    #Create Scenario
    material = RI.Vacuum()
    laser = MClass.Laser( length, material ) 
    scenario = MClass.Scenario(laser, k, r1, r2, freq=w/(2*np.pi))
#    scenario = MClass.Scenario(laser, k, r1, r2, freq=np.NaN)
#    scenario.set_w(w)
    
    #Initialize Maxwell Simulation
    Maxwell = MClass.Maxwell(N, simTime, scenario, event, method=method)
    Maxwell.set_initial_E(dist='zero') 
    Maxwell.event.updateEventStatus()

    #Plot Initial Amplitudes
    ax = MAX.plot([], [])
    x = Maxwell.get_x()
    plotInitialAmplitudes(x, Maxwell.event.get_E_left(), Maxwell.event.get_E_right(), ax)

    #Update fields "j" times
    for i in range(j):
        Maxwell.fieldAmplitudesUpdate()
        t = Maxwell.get_t()
        F[1:-1] = test.fun_f(x, t, w) 
        DtF[1:-1] = MAX.Dt_f(Maxwell.get_timeStep(), case='test')
        Maxwell.event.set_F( F )
        Maxwell.event.set_DtF( DtF )
        Maxwell.event.updateEventStatus()

    #Plot Final Amplitudes
    plotFinalAmplitudes(x, Maxwell.event.get_E_left(), Maxwell.event.get_E_right(), ax)
    plt.setp(ax.lines, linewidth=3.0)

    #Plot Final Total E field in new Figure
    E = Maxwell.get_E() 
    MAX.plot(x, E,"$x$","$E$", "Total electric field at time step %d"%j, text='Intervals: %d'%M)








